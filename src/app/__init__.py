from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from app.controller import api_controller
from config import PROD_DATABASE

def create_app():
    app = Flask(__name__)
    CORS(app)
    JWTManager(app)

    app.config['SQLALCHEMY_DATABASE_URI'] = PROD_DATABASE
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['JWT_SECRET_KEY'] = "W7NHYF3aWGVHFKkVMKPzZX8ZdNQgWAjCijlLMxdOCjSuLdXjSQX8xceg5TE0sdU"

    app.register_blueprint(api_controller)

    from app.models import db
    db.init_app(app)

    return app
