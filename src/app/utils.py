from flask import jsonify

def model_to_json(model):
    data = None
    if hasattr(model, '__iter__'):
        data = [e.serialize() for e in model]
    else:
        data = model.serialize()
    return jsonify(data)
