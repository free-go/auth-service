from flask import Blueprint, jsonify, request, abort, Response
from flask_jwt_extended import create_access_token, get_jwt_identity, verify_jwt_in_request

from app.models import UserAuth, db
from app.utils import model_to_json
from config import EXPOSED_ROUTES

api_controller = Blueprint('api_controller', __name__)
# EXPOSED_ROUTES defined in the config.py

@api_controller.route('/auth/login', methods=["POST"])
def login():
    if not request.is_json:
        abort(400)

    email = request.json.get('email', None)
    password = request.json.get('password', None)

    if not email or not password:
        abort(422)

    user = UserAuth.query.filter_by(email=email).first_or_404()
    if not user.check_password(password):
        return jsonify({"msg": "Invalid credentials"}), 401

    access_token = create_access_token(identity=user._id)
    return jsonify(access_token=access_token), 200

@api_controller.route('/auth/register', methods=["POST"])
def register():
    if not request.is_json:
        abort(400)

    email= request.json.get('email', None)
    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username or not password or not email:
        abort(422)

    if UserAuth.query.filter_by(email=email).first():
        abort(409)

    user = UserAuth(username, password, email)

    db.session.add(user)
    db.session.commit()

    return model_to_json(user), 201

@api_controller.route('/auth/check')
def check():
    # Not checking the JWT for exposed URLs (login, register ...)
    targetUrl = request.headers.get('X-Forwarded-Uri', '')
    auth = True
    for route in EXPOSED_ROUTES:
        if targetUrl.rstrip('/') == route:
            auth = False
            break

    if request.headers.get('X-Forwarded-Method', '') == "OPTIONS":
        auth = False

    resp = Response("")

    if auth:
        # Raise an exception if needed
        verify_jwt_in_request()

        # Set the user id in a header
        resp.headers['X-Auth-User'] = get_jwt_identity()

    return resp, 204

@api_controller.route('/auth/user/<int:user_id>')
def user_info(user_id):
    user = UserAuth.query.filter_by(_id=user_id).first_or_404()
    return model_to_json(user)
