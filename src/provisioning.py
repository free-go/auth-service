from app import create_app
from app.models import UserAuth, db

def provisioning(db):
    u1 = UserAuth("Vincent Couturier", "vincent", "vincent.couturier@cpe.fr")

    db.session.add(u1)
    db.session.commit()

if __name__ == '__main__':
    try:
        app = create_app()
        db.app = app
        db.drop_all()
        print("Database successfuly dropped !")
        db.create_all()
        print("Database successfuly created !")
        provisioning(db)
    except Exception as e:
        print(e)

