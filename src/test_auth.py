import sqlite3
import unittest
import json

from app import create_app
from app.models import db
from provisioning import provisioning
from config import TEST_DATABASE

class BasicTest(unittest.TestCase):
    def setUp(self):
        app = create_app()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = TEST_DATABASE
        self.app = app

        db.init_app(self.app)
        with self.app.app_context():
            db.drop_all()
            db.create_all()
            provisioning(db)
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_valid_login(self):
        data = json.dumps({'email': 'vincent.couturier@cpe.fr', 'password': 'vincent'})
        rv = self.app.post('/auth/login', data=data, content_type='application/json')
        assert rv.status_code == 200
        assert rv.json.get('access_token', False)

    def test_fail_login(self):
        data = json.dumps({'email': 'vincent.couturier@cpe.fr', 'password': 'fail'})
        rv = self.app.post('/auth/login', data=data, content_type='text/plain')
        assert rv.status_code == 400

        data = json.dumps({'email': 'vincent.couturier@cpe.fr', 'password': 'fail'})
        rv = self.app.post('/auth/login', data=data, content_type='application/json')
        assert rv.status_code == 401

        data = json.dumps({'email': 'test2@test.test', 'password': 'test2'})
        rv = self.app.post('/auth/login', data=data, content_type='application/json')
        assert rv.status_code == 404

        data = json.dumps({'username': 'test2@test.test', 'password': 'test2'})
        rv = self.app.post('/auth/login', data=data, content_type='application/json')
        assert rv.status_code == 422

    def test_valid_register(self):
        data = json.dumps({'email': 'test2@test.test', 'password': 'test2', 'username': 'test2'})
        rv = self.app.post('/auth/register', data=data, content_type='application/json')
        assert rv.status_code == 201

        data = json.dumps({'email': 'test2@test.test', 'password': 'test2'})
        rv = self.app.post('/auth/login', data=data, content_type='application/json')
        assert rv.status_code == 200

    def test_fail_register(self):
        data = json.dumps({'email': 'test2@test.test', 'password': 'test2', 'username': 'test2'})
        rv = self.app.post('/auth/register', data=data, content_type='text/plain')
        assert rv.status_code == 400

        data = json.dumps({'username': 'test2@test.test', 'password': 'test2'})
        rv = self.app.post('/auth/register', data=data, content_type='application/json')
        assert rv.status_code == 422

    def test_valid_check(self):
        data = json.dumps({'email': 'vincent.couturier@cpe.fr', 'password': 'vincent'})
        rv = self.app.post('/auth/login', data=data, content_type='application/json')
        token = rv.json['access_token']

        rv = self.app.get('/auth/check', headers={'Authorization': 'Bearer ' + token})
        assert rv.status_code == 204
        assert rv.headers.get('X-Auth-User', False)

    def test_ignore_check(self):
        rv = self.app.get('/auth/check', headers={'X-Forwarded-Uri': '/auth/login'})
        assert rv.status_code == 204

    def test_fail_check(self):
        token = "FAKE_TOKEN"
        rv = self.app.get('/auth/check', headers={'Authorization': 'Bearer ' + token})
        assert 400 <= rv.status_code < 500
