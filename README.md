# auth-service

Micro-service for authentication

## Docker

To build the docker & run it manually :

```bash
# In the project root folder
docker build -t auth-service .

# Port 5000 is Flask standard
docker run -d --name auth-service -p 5000:8080 auth-service
```

## JWT

Every request made to the backend is now being forwarded to this service, it will check whether or not you have a correct JWT in your headers, the header needed is as follow : `Authorization: Bearer {token}`.
This token will be provided after correctly querrying `/auth/login`, please note that `/auth/register` is not providing any token (you will need a second call to the login endpoint).  

If your token is invalid or if you don't have any token in your headers, you will get a 401 status, if it is valid you will get the requested page.

## Endpoints

METHOD | URL | REQUEST_BODY | RESPONSE_SUCCESS | STATUS_CODE
--- | --- | --- | --- | ---
`POST` | `/auth/login` | `{"email": "test@test.test", "password": "test"}` | `{"access_token": "TOKEN"}` | 200 (OK)<br />400 (NOT JSON)<br />401 (INVALID CREDENTIALS)<br />404 (USER NOT FOUND)<br />422 (BAD JSON)
`POST` | `/auth/register` | `{"email": "test2@test.test", "username": "test2", "password": "test2"}` | `{"email": "test2@test.test", "username": "test2"}` | 201 (CREATED)<br />400 (NOT JSON)<br />422 (BAD JSON)
